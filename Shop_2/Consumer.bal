import ballerinax/kafka;
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "Store_02",
    topics: ["Dsa_Assignment_2"],
    offsetReset: "earliest",
    pollingInterval: 1
};

public type Order record {
    int id;
    string customerNumber;
    Request[] request;
    string address;
};

public type Request record {
    int productId;
    int quantity;
};

// Create a subtype of `kafka:AnydataConsumerRecord`.
public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    remote function onConsumerRecord(OrderConsumerRecord[] records) returns error? {
        // The set of Kafka records received by the service are processed one by one.

        records.forEach(function(OrderConsumerRecord orderRecord) {
            log:printInfo("Received Valid Order: " + orderRecord.value.toString());
        });

    }
}
